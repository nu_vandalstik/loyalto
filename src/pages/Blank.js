/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from 'react';
import {ContentHeader} from '@components';
// import {useParams} from 'react-router';
import axios from 'axios';

const Blank = () => {
    const [detailFilm, setdetailFilm] = useState([]);
    // const {test} = props;
    // console.log(props.location.state.data);
    // console.log(test.location.state);
    const url = window.location.pathname;
    const id = url.substring(url.lastIndexOf('/') + 1);
    const ret = id.replace(':', '');
    console.log(ret);

    axios
        .get(
            `https://api.themoviedb.org/3/movie/${ret}?api_key=2fccde01a371b106b09a241d6d1d5b49`
        )
        .then((data) => {
            setdetailFilm([data.data]);
            console.log(data.data);
        });

    return (
        <div>
            <ContentHeader title="Blank Page" />
            <section className="content">
                {detailFilm.map((data) => {
                    return (
                        <div className="container-fluid">
                            <div className="card">
                                <div className="card-header">
                                    <h3
                                        className="card-title"
                                        style={{
                                            marginTop: 20,
                                            fontSize: 40,
                                            textAlign: 'center'
                                        }}
                                    >
                                        {data.original_title}
                                    </h3>

                                    <img
                                        src={`https://image.tmdb.org/t/p/w500${data.backdrop_path}`}
                                        className="card-img-top"
                                        alt="..."
                                    />
                                    <div className="card-tools">
                                        <button
                                            type="button"
                                            className="btn btn-tool"
                                            data-widget="collapse"
                                            data-toggle="tooltip"
                                            title="Collapse"
                                        >
                                            <i className="fa fa-minus" />
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-tool"
                                            data-widget="remove"
                                            data-toggle="tooltip"
                                            title="Remove"
                                        >
                                            <i className="fa fa-times" />
                                        </button>
                                    </div>
                                </div>
                                <div className="card-body">{data.overview}</div>
                                <div className="card-footer">
                                    release_date {data.release_date}
                                </div>
                            </div>
                        </div>
                    );
                })}
            </section>
        </div>
    );
};

export default Blank;
