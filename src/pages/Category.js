import React, {useEffect, useState} from 'react';
import axios from 'axios';

const Genres = () => {
    const [category, setCategory] = useState([]);

    const GetCategory = () =>
        axios
            .get(
                'https://api.themoviedb.org/3/genre/movie/list?api_key=2fccde01a371b106b09a241d6d1d5b49'
            )
            .then((data) => {
                setCategory(data.data.genres);
            })
            .catch((err) => err);
    useEffect(() => {
        GetCategory();
    }, [category]);

    // console.log(category);
    return (
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">First</th>
                </tr>
            </thead>
            <tbody>
                {category.map((data) => {
                    return (
                        <tr key={data.id}>
                            <th scope="row">{data.id}</th>
                            <td>{data.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};

export default Genres;
