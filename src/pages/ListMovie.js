import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

const ListMovie = () => {
    const [listMovie, setlistMovie] = useState([]);
    const [awal, setawal] = useState(1);
    const [page, setpage] = useState({
        totalPages: 1,
        totalResult: 1
    });

    const previous = () => (awal <= 1 ? 1 : setawal(awal - 1));

    const next = () =>
        awal === page.totalPages ? page.totalPages : setawal(awal + 1);

    const getLisitMovie = async () => {
        await axios
            .get(
                `https://api.themoviedb.org/3/movie/upcoming?api_key=2fccde01a371b106b09a241d6d1d5b49&page=${awal}`
            )
            .then((data) => {
                setlistMovie(data.data.results);
                setpage({
                    // pages: data.data.page,
                    totalPages: data.data.total_pages,
                    totalResult: data.data.total_results
                });
            });
    };
    // console.log(page);
    useEffect(() => {
        getLisitMovie();
        // setPage();
        // console.log(page);
        // console.log(dataMovie.total_pages);
    }, [page]);
    return (
        <div className="container">
            <div className="row align-items-start">
                {listMovie.map((data) => {
                    return (
                        <div key={data.id} className="col">
                            <div
                                className="card"
                                style={{width: 500, height: 650, marginTop: 10}}
                            >
                                <img
                                    src={`https://image.tmdb.org/t/p/w500${data.backdrop_path}`}
                                    className="card-img-top"
                                    alt="..."
                                />
                                <div className="card-body">
                                    <h5 className="card-title">
                                        {data.original_title}
                                    </h5>
                                    <p className="card-text">{data.overview}</p>
                                    <Link
                                        className="btn btn-primary"
                                        to={`/blank/:${data.id}`}
                                    >
                                        Go somewhere
                                    </Link>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <nav aria-label="Page navigation example">
                <ul className="pagination">
                    <li className="page-item">
                        <button
                            onClick={previous}
                            className="page-link"
                            aria-label="Previous"
                            type="submit"
                        >
                            <span aria-hidden="true">&laquo;</span>
                        </button>
                    </li>

                    <li className="page-item">
                        <p className="page-link">
                            {awal} / {page.totalPages}
                        </p>
                    </li>
                    <li className="page-item">
                        <button
                            className="page-link"
                            onClick={next}
                            type="submit"
                            aria-label="Next"
                        >
                            <span aria-hidden="true">&raquo;</span>
                        </button>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default ListMovie;
